<div class="col-lg-9 main-chart">

  <div class="row mtbox">

    <div class="col-md-2 col-sm-2 col-md-offset-2 box0">
      <a href="/assets">
        <div class="box1">
          <span class="li_banknote"></span>
          <h3><?=$this->Number->currency($totalValue,'USD',['precision'=>0])?></h3>
        </div>
        <p>Your assets are worth around <?=$this->Number->currency($totalValue,'USD',['precision'=>0])?>.</p>
      </a>
    </div>

    <div class="col-md-2 col-sm-2 box0">
      <a href="/assets">
        <div class="box1">
          <span class="li_like"></span>
          <h3><?=$assetCount?></h3>
        </div>
        <p>You have <?=$assetCount?> total assets.</p>
      </a>
    </div>

    <div class="col-md-2 col-sm-2 box0">
      <a href="/rooms">
        <div class="box1">
          <span class="li_shop"></span>
          <h3><?=$roomsCount?></h3>
        </div>
        <p>You have <?=$roomsCount?> total rooms.</p>
      </a>
    </div>

    <div class="col-md-2 col-sm-2 box0">
      <a href="/containers">
        <div class="box1">
          <span class="li_stack"></span>
          <h3><?=$containersCount?></h3>
        </div>
        <p>You have <?=$containersCount?> total containers.</p>
      </a>
    </div>

    <div class="col-md-2 col-sm-2 box0">
      <a href="/files/index">
        <div class="box1">
          <span class="li_camera"></span>
          <h3><?=$filesCount?></h3>
        </div>
        <p>You have <?=$filesCount?> total files.</p>
      </a>
    </div>

  </div><!-- /row mt -->	

</div>
