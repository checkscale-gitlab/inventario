<h3><i class="fa fa-angle-right"></i> Quick Add Items</h3>

<div class="col-lg-12">
    <div class="form-panel">
        <?= $this->Form->create(null,['class'=>'form-horizontal style-form']) ?>
            <div class="form-group">
                <label class="control-label col-md-3">Room</label>
                <div class="col-md-3 col-xs-11">
                  <?=$this->Form->control('room_id', ['options' => $rooms, 'empty' => '- Select -', 'label'=>false]);?>
                  <p> - or - </p>
                  <?=$this->Form->control('room_title', ['label'=>'New Room Title']);?>
                </div>
            </div>
            <div class="form-group tagsWrap">
                <label class="control-label col-md-3">Tags</label>
                <div class="col-md-3 col-xs-11">
                    <?php $tags = [];
                    foreach ($tagged as $tag) {
                        // debug($tag);
                        // $tagString .= $tag->tag->label.", ";
                        array_push($tags, $tag->tag->label);
                    }
                    ?>
                    <?=$this->Form->control('set_tags',['options'=>$tags,'multiple'=>'multiple']);?>

                    <span class="help-block">Optional.</span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Receipt Text</label>
                <div class="col-md-3 col-xs-11">
                  <?=$this->Form->textarea('text',['label'=>false]);?>
                  <span class="help-block">Paste an itemized receipt with UPC codes to import all items.</span>
                </div>
            </div>
            <?= $this->Form->button(__('Submit',['class'=>'btn btn-submit'])) ?>
            <?= $this->Form->end() ?>
        </form>
    </div><!-- /form-panel -->
</div>