<?php
namespace App\Model\Table;

use CakeDC\Users\Model\Table\UsersTable;
use Cake\ORM\TableRegistry;

/**
 * Application specific Users Table with non plugin conform field(s)
 */
class MyUsersTable extends \CakeDC\Users\Model\Table\UsersTable
{
  public function afterSave($event, $entity, $options) {
    $Rooms = TableRegistry::get('Rooms');
    $defaultRooms = [
      'Bedroom',
      'Bathroom',
      'Living Room',
      'Kitchen'
    ];
    foreach ($defaultRooms as $roomName) {
      $room = $Rooms->newEntity();
      $room->title = $roomName;
      $room->user_id = $entity->id;
      $Rooms->save($room);
    }
  }
}